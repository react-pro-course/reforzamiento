import { useReducer } from "react";
import { CounterState } from "./interfaces/interface";
import counterReducer from "./state/counterReducer";
import { doReset, doIncreaseBy } from "./actions/actions";

const INITIAL_STATE: CounterState = {
  counter: 0,
  previous: 0,
  changes: 0,
};

const CounterReducer = () => {
  const [state, dispatch] = useReducer(counterReducer, INITIAL_STATE);

  const increaseBy = (value: number) => dispatch(doIncreaseBy(value));

  const handleReset = () => {
    dispatch(doReset());
  };

  return (
    <>
      <h1>CounterReducer segementado: {state.counter}</h1>
      <button onClick={() => increaseBy(1)}>+1</button>
      <button onClick={handleReset}>Reset</button>
      <button onClick={() => increaseBy(5)}>+5</button>
      <button onClick={() => increaseBy(10)}>+10</button>
    </>
  );
};

export default CounterReducer;
