import { CounterState } from "../interfaces/interface";
import { CounterAction, actions } from "../actions/actions";

const counterReducer = (
  state: CounterState,
  action: CounterAction
): CounterState => {
  const { counter } = state;

  switch (action.type) {
    case actions.INCREASE_BY: {
      const { value } = action.payload;

      return {
        changes: counter + 1,
        counter: value + counter,
        previous: counter,
      };
    }
    case actions.RESET:
      return {
        changes: 0,
        counter: 0,
        previous: 0,
      };
    default:
      return state;
  }
};

export default counterReducer;
