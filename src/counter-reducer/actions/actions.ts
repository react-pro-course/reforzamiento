// actions

interface Actions {
  RESET: "RESET";
  INCREASE_BY: "INCREASE_BY";
}

export const actions: Actions = {
  RESET: "RESET",
  INCREASE_BY: "INCREASE_BY",
};

export type CounterAction =
  | {
      type: typeof actions.INCREASE_BY;
      payload: {
        value: number;
      };
    }
  | { type: typeof actions.RESET };

// action creators
export const doReset = (): CounterAction => ({
  type: actions.RESET,
});

export const doIncreaseBy = (value: number): CounterAction => ({
  type: actions.INCREASE_BY,
  payload: {
    value,
  },
});
