import { useReducer } from "react";

interface CounterState {
  counter: number;
  previous: number;
  changes: number;
}

const INITIAL_STATE: CounterState = {
  counter: 0,
  previous: 0,
  changes: 0,
};

type CounterAction =
  | {
      type: "increaseBy";
      payload: {
        value: number;
      };
    }
  | { type: "reset" };

const reducer = (state: CounterState, action: CounterAction): CounterState => {
  const { counter } = state;

  switch (action.type) {
    case "increaseBy": {
      const { value } = action.payload;

      return {
        changes: counter + 1,
        counter: value + counter,
        previous: counter,
      };
    }
    case "reset":
      return INITIAL_STATE;
    default:
      return state;
  }
};

const CounterReducer = () => {
  const [state, dispatch] = useReducer(reducer, INITIAL_STATE);

  const increaseBy = (value: number) =>
    dispatch({
      type: "increaseBy",
      payload: {
        value,
      },
    });

  const handleReset = () => {
    dispatch({
      type: "reset",
    });
  };

  return (
    <>
      <h1>CounterReducer: {state.counter}</h1>
      <button onClick={() => increaseBy(1)}>+1</button>
      <button onClick={handleReset}>Reset</button>
      <button onClick={() => increaseBy(5)}>+5</button>
      <button onClick={() => increaseBy(10)}>+10</button>
    </>
  );
};

export default CounterReducer;
