import { FC, useState } from "react";

interface CounterProps {
  initialValue: number;
}

const Counter: FC<CounterProps> = ({ initialValue = 0 }) => {
  const [counter, setCounter] = useState(initialValue);

  const handleClick = () => {
    setCounter((prevState) => prevState + 1);
  };

  return (
    <>
      <h1>Counter: {counter}</h1>
      <button onClick={handleClick}> + 1</button>
    </>
  );
};

export default Counter;
