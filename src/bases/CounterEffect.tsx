import { useEffect, useRef, useState } from "react";
import { gsap } from "gsap";

const MAXIMUN_COUNT = 10;

const CounterEffect = () => {
  const [counter, setCounter] = useState(5);

  const counterElement = useRef<HTMLHeadingElement>(null);

  const handleClick = () => {
    if (counter >= MAXIMUN_COUNT) return;

    setCounter((prevState) => prevState + 1);
  };

  useEffect(() => {
    if (counter < MAXIMUN_COUNT) return;

    console.log(
      "%c se llegó al valor máximo",
      "color: red; background-color: white"
    );

    const tl = gsap.timeline();

    tl.to(counterElement.current, {
      fontSize: 30,
      duration: 0.2,
      ease: "ease.out",
    });

    tl.to(counterElement.current, {
      fontSize: 20,
      duration: 1,
      ease: "bounce.out",
    });
  }, [counter]);

  return (
    <>
      <h1>CounterEffect:</h1>
      <h2 ref={counterElement} className="counter">
        {counter}
      </h2>
      <button onClick={handleClick}> + 1</button>
    </>
  );
};

export default CounterEffect;
