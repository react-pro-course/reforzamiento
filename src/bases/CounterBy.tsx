import { FC, useState } from "react";

interface CounterProps {
  initialValue?: number;
}

interface CounterByState {
  counter: number;
  clicks: number;
}

const CounterBy: FC<CounterProps> = ({ initialValue = 5 }) => {
  const [counter, setCounter] = useState<CounterByState>({
    counter: initialValue,
    clicks: 0,
  });

  const handleClick = (value: number = 0) => {
    setCounter(({ counter, clicks }) => ({
      counter: counter + value,
      clicks: clicks + 1,
    }));
  };

  return (
    <>
      <h1>CounterBy: {counter.counter}</h1>
      <h1>Clicks: {counter.clicks}</h1>

      <button onClick={() => handleClick(1)}> +1</button>
      <button onClick={() => handleClick(5)}> +5</button>
    </>
  );
};

export default CounterBy;
